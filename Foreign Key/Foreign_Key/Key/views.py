from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
from . models import *

### Admin Page ######
def admin_home(request):
    x1=Project.objects.all()
    return render(request,'admin_home.html',{'x1':x1})

def add_project(request):
    if request.method=="POST":
        x2=Project()
        x2.Projectname=request.POST.get("Projectname")
        x2.save()
        return redirect("http://192.168.2.64:9001/admin-home")
    else:
        return render(request,'add_project.html')

def edit_project(request,id):
    if request.method=="POST":
        x3=Project.objects.get(id=id)
        x3.Projectname=request.POST.get("Projectname")
        x3.save()
        return redirect("http://192.168.2.64:9001/admin-home")
    else:
        x4=Project.objects.get(id=id)
        return render(request,'edit_project.html',{'x4':x4})

def delete_project(request,id):
    x5=Project.objects.get(id=id)
    x5.delete()
    return redirect("http://192.168.2.64:9001/admin-home")

def view_project(request,id):
    x7=get_object_or_404(Project,id=id)
    x6=ProjectDetails.objects.filter(P_Name=x7)
    print(x7.Projectname,'*************')
    return render(request,'view_project.html',{'x6':x6 , 'x7':x7})
def AddMember(request,id):
    if request.method=="POST":
        ab=Members()
        ab.Projectname=request.POST.get("Projectname")
        ab.Membername=request.POST.get("Membername")
        ab.Designation=request.POST.get("Designation")
        ab.Domain=request.POST.get("Domain")
        ab.save()
        return redirect("http://192.168.2.64:9001/AddMember/%s"%id)
    else:
        x9=Project.objects.get(id=id)
        x10=Members.objects.filter(Projectname=x9.Projectname)
        return render(request,"task2.html",{'x9':x9 , 'x10':x10})

def Assign_Member(request,id):
    x9=Project.objects.get(id=id)
    x10=ProjectDetail.objects.filter(Projectname=x9.Projectname)
   
    return render(request,'member_home2.html',{'y1':x10,'x9':x9})

def Delete_Member(request,id):
    d1=Members.objects.get(id=id)
    d1.delete()
    a=Project.objects.filter(Projectname=d1.Projectname)
    for i in a:
        b=i.id
    return redirect("http://192.168.2.64:9001/AddMember/%s"%b)

def Update_Member(request,id):
    if request.method=="POST":
        d2=Members.objects.get(id=id)
        d2.Membername=request.POST.get("Membername")
        d2.Designation=request.POST.get("Designation")
        d2.Domain=request.POST.get("Domain")
        d2.save()
        a=Project.objects.filter(Projectname=d2.Projectname)
        for i in a:
            b=i.id
        print(b,'##################')
        return redirect("http://192.168.2.64:9001/AddMember/%s"%b)
    else:
        d3=Members.objects.get(id=id)
        return render(request,'Update_Member.html',{'d3':d3})
   

    
def AddTask(request):
    if request.method=="POST":
        x8=ProjectDetail()
        x8.Projectname=request.POST.get("Projectname")
        x8.Task=request.POST.get("Task")
        x8.TaskDescription=request.POST.get("TaskDescription")
        x8.Starting_date=request.POST.get("Starting_date")
        x8.Ending_date=request.POST.get("Ending_date")
        x8.save()
       
        return HttpResponse("updated")
    else:
       
        return render(request,"AddTask.html")

def search(request):
    a=request.GET.get("Projectname1")
    b=Project.objects.filter(Projectname=a)
    if b:
        return render(request,'admin_home.html',{'b':b})
    else:
        c="<script> window.alert('No data')</script>"
        return HttpResponse(c)


def add_task(request,id):
    if request.method=="POST":
        x8=ProjectDetails()
        x8.Projectname=request.POST.get("Projectname")
        x8.Task=request.POST.get("Task")
        x8.TaskDescription=request.POST.get("TaskDescription")
        x8.Starting_date=request.POST.get("Starting_date")
        x8.Ending_date=request.POST.get("Ending_date")
        x8.save()
       
        return redirect("http://192.168.2.64:9001/view_project/%s"% id)
    else:
        x9=Project.objects.get(id=id)
        return render(request,"add_task.html",{'x9':x9})

def edit_task(request,id):
    if request.method=="POST":
        x10=ProjectDetail.objects.get(id=id)
        x10.Task=request.POST.get("Task")
        x10.TaskDescription=request.POST.get("TaskDescription")
        x10.Starting_date=request.POST.get("Starting_date")
        x10.Ending_date=request.POST.get("Ending_date")
        x10.save()
        c=Project.objects.filter(Projectname=x10.Projectname)
        for i in c:
            b=i.id
        return redirect("http://192.168.2.64:9001/view_project/%s"% b)
    else:
        x11=ProjectDetail.objects.get(id=id)
        return render(request,'edit_task.html',{'x11':x11})
def delete_task(request,id):
    
    x12=ProjectDetail.objects.get(id=id)
    x12.delete()
    c=Project.objects.filter(Projectname=x12.Projectname)
    for i in c:
            b=i.id
    return redirect("http://192.168.2.64:9001/view_project/%s"% b)
    

    

def track_task(request,id):
    x13=Project.objects.get(id=id)
    x14=get_object_or_404(Project,id=id)
    x15=ProjectDetail.objects.filter(Projectname=x13.Projectname)
    return render(request,'track_task.html',{'x14':x14,'x15':x15})


################### Member Portal ####################


def member_home(request):
    y1=ProjectDetail.objects.all()
    return render(request,'member_home.html',{'y1':y1})

def view_member(request,id):
    y2=get_object_or_404(ProjectDetail,id=id)
    y3=ProjectDetail.objects.filter(Membername=y2.Membername)
    y5=ProjectDetail.objects.filter(Testing_Status="Not Completed")
    y9=ProjectDetail.objects.get(id=id)
    y8=ProjectDetail.objects.filter(Membername=y9.Membername, Testing_Status="Not Completed")
    if request.method=="POST":
        y4=ProjectDetail.objects.get(id=id)
        y4.Status=request.POST.get("Status")
        y4.StatusDescription=request.POST.get("StatusDescription")
        y4.save()
        if (y4.Status=="Completed"):
            y4.Testing_Status=""
            y4.save()
        return redirect("http://192.168.2.64:9001/view_member/%s"%id)


    return render(request,'view_member.html',{'y3':y3 ,'y2':y2 , 'y8':y8})

############## Testing Team Portal##################

def tester_home(request):
    #z1=Project.objects.all()
    #return render(request,'tester_home.html',{'z1':z1})
    z1=ProjectDetail.objects.all()
    return render(request,'tester_home2.html',{'z1':z1})
def view_tester(request,id):
    if request.method=="POST":      
        z4=ProjectDetail.objects.get(id=id)
        
        z4.DebugDescription=request.POST.get("DebugDescription")
        z4.Testing_Status=request.POST.get("Testing_Status")
        z4.save()
        if (z4.Testing_Status=="Not Completed"):
            z4.Status="Returned from Tester"
            z4.save()
            
        return redirect("http://192.168.2.64:9001/view_tester/%s"%id)
        
       

    else:
        z5=ProjectDetail.objects.get(id=id)
        z3=ProjectDetail.objects.filter(Task=z5.Task, Status="Completed")
        
        return render(request,'view_tester.html',{'z3':z3})
def test_completed(request):
    z6=ProjectDetail.objects.filter(Testing_Status="Completed")
    return render(request,'test_completed.html',{'z6':z6})

def all_delete(request):
    d1=ProjectDetail.objects.all()
    d2=Project.objects.all()
    d3=Members.objects.all()
    d1.delete()
    d2.delete()
    d3.delete()
    return HttpResponse("All Deleted")
    
##################################################################

def task_list(request):
    y1=ProjectDetail.objects.all()
    return render(request,'member_home1.html',{'y1':y1})

def add_member(request,id):
    if request.method=="POST":
        y2=ProjectDetail.objects.get(id=id)
        y2.Membername=request.POST.get("Membername")
        y2.save()
        y3=Project.objects.filter(Projectname=y2.Projectname)
        for i in y3:
            y4=i.id
        return redirect("http://192.168.2.64:9001/Assign_Member/%s"%y4)
    else:
        y3=ProjectDetail.objects.get(id=id)
        y4=Members.objects.filter(Projectname=y3.Projectname)
        y5=Project.objects.filter(Projectname=y3.Projectname)
        for i in y5:
            y6=i
        mylist=[]
        a=Members.objects.filter(Projectname=y3.Projectname)
        for i in a:
            mylist.append(i.Membername)
        
        return render(request,'add_member.html',{'y3':y3 ,'mylist':mylist,'y4':y4 ,'y6':y6})

def edit_member(request,id):
    if request.method=="POST":
        y2=ProjectDetail.objects.get(id=id)
        y2.Membername=request.POST.get("Membername")
        y2.save()
        y3=Project.objects.filter(Projectname=y2.Projectname)
        for i in y3:
            y4=i.id
       
        return redirect("http://192.168.2.64:9001/Assign_Member/%s"%y4)
    else:
        y3=ProjectDetail.objects.get(id=id)
        mylist=[]
        a=Members.objects.filter(Projectname=y3.Projectname)
        for i in a:
            mylist.append(i)
        #print(mylist)

        return render(request,'edit_member.html',{'y3':y3 ,'mylist':mylist })










