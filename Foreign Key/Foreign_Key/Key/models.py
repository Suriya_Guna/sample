from django.db import models

class Project(models.Model):
    Projectname=models.CharField(max_length=1000)

class Member(models.Model):
    Pname=models.ForeignKey(Project,on_delete=models.CASCADE)
    Membername=models.CharField(max_length=1000)
    Designation=models.CharField(max_length=1000)
    Domain=models.CharField(max_length=1000)
class ProjectDetails(models.Model):
    P_Name=models.ForeignKey(Project,on_delete=models.CASCADE)
    M_Name=models.ForeignKey(Member,on_delete=models.CASCADE)
    Task=models.CharField(max_length=1000)
    TaskDescription=models.CharField(max_length=10000)
    Starting_date=models.CharField(max_length=100)
    Ending_date=models.CharField(max_length=100)
    Status=models.CharField(max_length=1000)
    StatusDescription=models.CharField(max_length=10000)
    Task=models.CharField(max_length=1000)
    DebugDescription=models.CharField(max_length=10000)
    Testing_Status=models.CharField(max_length=1000)