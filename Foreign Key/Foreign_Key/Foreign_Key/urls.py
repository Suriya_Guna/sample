"""Foreign_Key URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Key import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin-home',views.admin_home),
    path('add_project',views.add_project),
    path('edit_project/<int:id>',views.edit_project),
    path('delete_project/<int:id>',views.delete_project),
    path('view_project/<int:id>',views.view_project),
    path('add_task/<int:id>',views.add_task),
    path('AddTask',views.AddTask),
    path('edit_task/<int:id>',views.edit_task),
    path('delete_task/<int:id>',views.delete_task),
    path('search',views.search),
    path('member_home',views.member_home),
    path('view_member/<int:id>',views.view_member),
    path('track_task/<int:id>',views.track_task),
    path('tester_home',views.tester_home),
    path('view_tester/<int:id>',views.view_tester),
    path('test_completed',views.test_completed),
    path('all_delete',views.all_delete),
    path('task_list',views.task_list),
    path('add_member/<int:id>',views.add_member),
    path('edit_member/<int:id>',views.edit_member),
    path('AddMember/<int:id>',views.AddMember),
    path('Assign_Member/<int:id>',views.Assign_Member),
    path('Delete_Member/<int:id>',views.Delete_Member),
    path('Update_Member/<int:id>',views.Update_Member),
   
]
